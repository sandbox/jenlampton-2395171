<?php

/**
 * Implements template_preprocess_html().
 *
 */
function badcamp_preprocess_html(&$variables) {
 // Add conditional CSS for IE. To use uncomment below and add IE css file
 // drupal_add_css(path_to_theme() . '/css/ie.css', array('weight' => CSS_THEME, 'browsers' => array('!IE' => FALSE), 'preprocess' => FALSE));
 drupal_add_css('//fonts.googleapis.com/css?family=Press+Start+2P&subset=latin');
 drupal_add_js(path_to_theme().'/js/vendor/loginoverlay.js',
    array('scope' => 'footer', 'weight' => 6)
  );
  drupal_add_js(path_to_theme().'/js/vendor/classie.js',
    array('scope' => 'footer', 'weight' => 5)
  );

 // Need legacy support for IE downgrade to Foundation 2 or use JS file below
 // drupal_add_js('http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js', 'external');
}

/**
 * Implements template_preprocess_page
 *
 */
//function badcamp_preprocess_page(&$variables) {
//}

/**
 * Implements template_preprocess_node
 *
 */
//function badcamp_preprocess_node(&$variables) {
//}

function badcamp_preprocess_panels_pane(&$var) {
	
	$vars['pane_prefix'] = "<div class='monkey'>";
	$vars['pane_suffix'] = "</div>";
// dpm($var);
}

/**
 * Override theme_views_view_grouping() to get the right html and classes for the session schedule view title
 * Display a single views grouping.
 */
function badcamp_views_view_grouping($vars) {
  $view = $vars['view'];
  $title = $vars['title'];
  $content = $vars['content'];

  $output = '<div class="view-grouping">';
  if ($view->name == "sessions" && $view->current_display == "panel_pane_3") {
    $output .= '<h3 class="view-grouping-header">' . $title . '</h3>';
  }
  else {
    $output .= '<div class="view-grouping-header">' . $title . '</div>';
  }
  $output .= '<div class="view-grouping-content">' . $content . '</div>' ;
  $output .= '</div>';

  return $output;
}

function badcamp_preprocess_page(&$vars) {
  if (isset($vars['page']['content']['system_main']['#bundle']) && $vars['page']['content']['system_main']['#bundle'] == 'summit_speaker_application') {
    $vars['page']['content']['system_main']['#attributes']['class'][] = 'medium-6 medium-offset-4 large-4 large-offset-4 columns';
  }
}