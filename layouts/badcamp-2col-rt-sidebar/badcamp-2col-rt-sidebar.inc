<?php
// Plugin definition for the layout
$plugin = array(
  'title' => t('BADCamp 2 Column Right Sidebar'),
  'icon' => 'badcamp-2col-rt-sidebar.png',
  'category' => t('BADCamp'),
  'theme' => 'badcamp-2col-rt-sidebar',
  'regions' => array(
    'left' => t('Left'),
    'right' => t('Right'),
  ),
);
